<?php
/**
 * Copyright (c) 2022, MOBICOOP. All rights reserved.
 ***************************
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 ***************************
 *    Licence MOBICOOP described in the file
 *    LICENSE
 **************************/

/******************************************************************************

    Home page

******************************************************************************/



/* Load configuration */
require_once('config.dist.php');
include('config.local.php');


require_once('api-functions.inc.php');

require 'vendor/autoload.php';


use GuzzleHttp\Client;


/*
    Initialize DB
*/

$mysqli = new mysqli($DB_SERVER, $DB_USERNAME, $DB_PASSWORD, $DB_DBNAME, $DB_PORT);

$mysqli->query("DROP TABLE IF EXISTS proposal");

$queryText = <<<EOT
CREATE TABLE IF NOT EXISTS `proposal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `origin_lat` DECIMAL(10,6) NOT NULL,
  `origin_long` DECIMAL(10,6) NOT NULL,
  `origin_name` VARCHAR(255) NOT NULL,
  `destination_lat` DECIMAL(10,6) NOT NULL,
  `destination_long` DECIMAL(10,6) NOT NULL,
  `destination_name` VARCHAR(255) NOT NULL,
  `role` INT NULL,
  PRIMARY KEY (`id`));

EOT;

$mysqli->query($queryText);



/*
    Load database from API
*/

/* Connect and get token*/
$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => $API_BASE_URI,
    // You can set any number of default request options.
    'timeout'  => 5.0,
]);

$response = $client->request('POST', 'login', [
    'json' => [ 'username' => $API_USERNAME,
                'password' => $API_PASSWORD]
]);
/*
$response = $client->request('POST', 'login', [
    'json' => [ 'username_delegate' => 'paloma.ponctuel@yopmail.com',
                'username' => $API_USERNAME,
                'password' => $API_PASSWORD]
]);
*/

if ($response->getStatusCode() == 200) {
    $token = json_decode($response->getBody())->token;  
}



try {
    $response = $client->request('GET', 'carpools/1', [
        'headers' => [
            'Accept'     => 'application/ld+json',
            'Content-Type'     => 'application/json',
            'authorization'      => 'Bearer '.$token,
            'Connection'     => 'keep-alive'],
        'json' => [ 'perPage' => 10],
        'timeout' => 5
    ]);
/*
    $response = $client->request('GET', 'users/1/proposals_delegates', [
        'headers' => [
            'Accept'     => 'application/ld+json',
            'Content-Type'     => 'application/json',
            'authorization'      => 'Bearer '.$token,
            'Connection'     => 'keep-alive'],
        'json' => [ 'private' => true,
                    'criteria.fromDate' => '2021-11-20',
                    'perPage' => 10]
    ]);
*/
} catch(Exception $e) {
   var_dump($e);

}






mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

try {
    $stmt = $mysqli->prepare("INSERT INTO proposal(origin_lat, origin_long, origin_name, destination_lat, destination_long, destination_name, role) VALUES (?, ?, ?, ?, ?, ?, ?);");
    
    $params = [1,1,"a",2,2,"b",0];
    $types = 'ddsddsi';

    //$stmt->bind_param("is", $origin_lat, $origin_long, $origin_name, $destination_lat, $destination_long, $destination_name);
    $stmt->bind_param($types, ...$params);

    $adIdMin = 40;
    $adIdMax = 50;

    for($adId=$adIdMin; $adId <= $adIdMax; $adId++) {
        echo "Ad #$adId... ";
            
        // Get the ad matching the given id
        $response = $client->request('GET', 'carpools/'.$adId, [
            'headers' => [
                'Accept'     => 'application/ld+json',
                'Content-Type'     => 'application/json',
                'authorization'      => 'Bearer '.$token,
                'Connection'     => 'keep-alive'],
                'timeout' => 5
        ]);

//role = 1 : driver
        // If an ad exists, add it to the local database
        if ($response->getStatusCode() == 200) {
            echo "found... ";
            $adJsonObject = json_decode($response->getBody());

            // Test if is a search
            if ($adJsonObject->search == TRUE) {
                echo "but rejected because is a search.\n";     
            } else {
                // Test if regular
                if ($adJsonObject->frequency == 2) {
                    // Test if driver (role=1) or anyhow (role=3)
                    if ($adJsonObject->role == 1 || $adJsonObject->role == 3) {
                        $params[0] = blurCoordinate($adJsonObject->outwardWaypoints[0]->latitude);
                        $params[1] = blurCoordinate($adJsonObject->outwardWaypoints[0]->longitude);
                        $params[2] = $adJsonObject->outwardWaypoints[0]->addressLocality;
                        $params[3] = blurCoordinate($adJsonObject->outwardWaypoints[1]->latitude);
                        $params[4] = blurCoordinate($adJsonObject->outwardWaypoints[1]->longitude);
                        $params[5] = $adJsonObject->outwardWaypoints[1]->addressLocality;
                        $params[6] = $adJsonObject->role;
                    
                        if ($stmt->execute()) {
                            echo "and inserted.\n";
                        } else {
                            echo "and NOT inserted!!!\n";
                        }
                    } else {
                        echo "but rejected because not 'driver' or 'anyhow'.\n";               
                    }
                } else {
                    echo "but rejected because not a regular ad.\n";         
                }  
            }
        } else {
            echo "not found.\n";
        }
        
        
    }


} catch(Exception $e) {
   var_dump($e);

}



?>
