<!doctype html>
<?php

$territory_id = $_GET["territory_id"];

$territoriesArray[528]['campaignTitle'] = "Devenez un rossignol, la suite!";
$territoriesArray[528]['name'] = "CC des Baronnies en Drôme Provençale";
$territoriesArray[528]['objective'] = 211;
$territoriesArray[528]['centerLat'] = 44.3;
$territoriesArray[528]['centerLong'] = 5.3645;
$territoriesArray[528]['mapDefaultZoom'] = 10;
// $territoriesArray[528]['pdfMoreInformation'] = "Prospectus-DevenezUnRossignol.pdf";
// $territoriesArray[528]['startDate'] = "2022-12-01";
// $territoriesArray[528]['endDate'] = "2023-01-31";

$territoriesArray[777]['campaignTitle'] = "";
$territoriesArray[777]['name'] = "CC du Val de Somme";
$territoriesArray[777]['objective'] = NULL;
$territoriesArray[777]['centerLat'] = 49.908;
$territoriesArray[777]['centerLong'] = 2.5111;
$territoriesArray[777]['mapDefaultZoom'] = 10;
//Id 226 sur test
$territoriesArray[226]['name'] = "Meurthe-et-Moselle";
$territoriesArray[226]['objective'] = 11;
$territoriesArray[226]['centerLat'] = 48.788;
$territoriesArray[226]['centerLong'] = 6.162;
$territoriesArray[226]['mapDefaultZoom'] = 9;


/* Communes du 528 */
$territoriesArray[528]['children'] = "9941,17353,23695,26805,19237,7752,15581,29977,29181,26824,13176,15578,13833,4410,32542,21871,15436,25589,34729,32200,21382,24402,18358,21237,30771,25352,21288,4032,22509,26485,18060,28702,5940,15770,28512,24233,6383,26726,13604,15750,9896,13685,9468,24781,23960,12394,12367,33363,25641,20230,12764,32607,28250,26487,35991,19438,34362,9946,32592,35042,26414,8702,26512,29366,22970,5068,14652,26663,31011,35832,25719,23012,1994,9177,24630,17368,33608,27581,28169,21789,25676,14120,24920,21101,26104,7019,24618,35266,18856,10866,35466,16168,20444,2738,26556,10064,18719,7755,21444,31281,1952,29385,35991";


$territoriesArray[1952]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[1952]['name'] = "Villeperdrix";	$territoriesArray[1952]['objective'] = "4";	$territoriesArray[1952]['centerLat'] = 44.45557724863651;	$territoriesArray[1952]['centerLong'] = 5.29978961511157;	$territoriesArray[1952]['mapDefaultZoom'] = 11;	
$territoriesArray[1994]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[1994]['name'] = "Saint-André-de-Rosans";	$territoriesArray[1994]['objective'] = "4";	$territoriesArray[1994]['centerLat'] = 44.36449065793591;	$territoriesArray[1994]['centerLong'] = 5.521258132845277;	$territoriesArray[1994]['mapDefaultZoom'] = 11;	
$territoriesArray[2738]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[2738]['name'] = "Verclause";	$territoriesArray[2738]['objective'] = "4";	$territoriesArray[2738]['centerLat'] = 44.395474125221966;	$territoriesArray[2738]['centerLong'] = 5.407354024388085;	$territoriesArray[2738]['mapDefaultZoom'] = 11;	
$territoriesArray[4032]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[4032]['name'] = "Faucon";	$territoriesArray[4032]['objective'] = "5";	$territoriesArray[4032]['centerLat'] = 44.25234331700153;	$territoriesArray[4032]['centerLong'] = 5.142943082824573;	$territoriesArray[4032]['mapDefaultZoom'] = 11;	
$territoriesArray[4410]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[4410]['name'] = "Brantes";	$territoriesArray[4410]['objective'] = "4";	$territoriesArray[4410]['centerLat'] = 44.185309597018;	$territoriesArray[4410]['centerLong'] = 5.332131060919648;	$territoriesArray[4410]['mapDefaultZoom'] = 11;	
$territoriesArray[5068]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[5068]['name'] = "Rioms";	$territoriesArray[5068]['objective'] = "4";	$territoriesArray[5068]['centerLat'] = 44.269868731752716;	$territoriesArray[5068]['centerLong'] = 5.460541204238585;	$territoriesArray[5068]['mapDefaultZoom'] = 11;	
$territoriesArray[5940]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[5940]['name'] = "La Penne-sur-l'Ouvèze";	$territoriesArray[5940]['objective'] = "4";	$territoriesArray[5940]['centerLat'] = 44.250633067442266;	$territoriesArray[5940]['centerLong'] = 5.233092749262294;	$territoriesArray[5940]['mapDefaultZoom'] = 11;	
$territoriesArray[6383]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[6383]['name'] = "Lachau";	$territoriesArray[6383]['objective'] = "4";	$territoriesArray[6383]['centerLat'] = 44.21612647138824;	$territoriesArray[6383]['centerLong'] = 5.65184517640839;	$territoriesArray[6383]['mapDefaultZoom'] = 11;	
$territoriesArray[7019]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[7019]['name'] = "Séderon";	$territoriesArray[7019]['objective'] = "3";	$territoriesArray[7019]['centerLat'] = 44.19753966108892;	$territoriesArray[7019]['centerLong'] = 5.554538245156938;	$territoriesArray[7019]['mapDefaultZoom'] = 11;	
$territoriesArray[7752]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[7752]['name'] = "Ballons";	$territoriesArray[7752]['objective'] = "4";	$territoriesArray[7752]['centerLat'] = 44.25142656147016;	$territoriesArray[7752]['centerLong'] = 5.644370918634405;	$territoriesArray[7752]['mapDefaultZoom'] = 11;	
$territoriesArray[7755]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[7755]['name'] = "Villebois-les-Pins";	$territoriesArray[7755]['objective'] = "4";	$territoriesArray[7755]['centerLat'] = 44.319858647132925;	$territoriesArray[7755]['centerLong'] = 5.597241121397463;	$territoriesArray[7755]['mapDefaultZoom'] = 11;	
$territoriesArray[8702]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[8702]['name'] = "Puyméras";	$territoriesArray[8702]['objective'] = "6";	$territoriesArray[8702]['centerLat'] = 44.285087244978534;	$territoriesArray[8702]['centerLong'] = 5.136721278905512;	$territoriesArray[8702]['mapDefaultZoom'] = 11;	
$territoriesArray[9177]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[9177]['name'] = "Saint-Auban-sur-l'Ouvèze";	$territoriesArray[9177]['objective'] = "4";	$territoriesArray[9177]['centerLat'] = 44.29730507076101;	$territoriesArray[9177]['centerLong'] = 5.429672327535131;	$territoriesArray[9177]['mapDefaultZoom'] = 11;	
$territoriesArray[9468]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[9468]['name'] = "Mérindol-les-Oliviers";	$territoriesArray[9468]['objective'] = "4";	$territoriesArray[9468]['centerLat'] = 44.272797507526924;	$territoriesArray[9468]['centerLong'] = 5.167993929692696;	$territoriesArray[9468]['mapDefaultZoom'] = 11;	
$territoriesArray[9896]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[9896]['name'] = "Les Omergues";	$territoriesArray[9896]['objective'] = "4";	$territoriesArray[9896]['centerLat'] = 44.152398670419466;	$territoriesArray[9896]['centerLong'] = 5.5890930562458045;	$territoriesArray[9896]['mapDefaultZoom'] = 11;	
$territoriesArray[9941]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[9941]['name'] = "Arnayon";	$territoriesArray[9941]['objective'] = "4";	$territoriesArray[9941]['centerLat'] = 44.493348453729915;	$territoriesArray[9941]['centerLong'] = 5.3113958558249035;	$territoriesArray[9941]['mapDefaultZoom'] = 11;	
$territoriesArray[9946]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[9946]['name'] = "Pierrelongue";	$territoriesArray[9946]['objective'] = "4";	$territoriesArray[9946]['centerLat'] = 44.249711908595394;	$territoriesArray[9946]['centerLong'] = 5.214502705733801;	$territoriesArray[9946]['mapDefaultZoom'] = 11;	
$territoriesArray[10064]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[10064]['name'] = "Vers-sur-Méouge";	$territoriesArray[10064]['objective'] = "4";	$territoriesArray[10064]['centerLat'] = 44.235513010213936;	$territoriesArray[10064]['centerLong'] = 5.552061415736882;	$territoriesArray[10064]['mapDefaultZoom'] = 11;	
$territoriesArray[10866]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[10866]['name'] = "Valdoule";	$territoriesArray[10866]['objective'] = "4";	$territoriesArray[10866]['centerLat'] = 44.459614203554615;	$territoriesArray[10866]['centerLong'] = 5.521786301452676;	$territoriesArray[10866]['mapDefaultZoom'] = 11;	
$territoriesArray[12367]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[12367]['name'] = "Montauban-sur-l'Ouvèze";	$territoriesArray[12367]['objective'] = "4";	$territoriesArray[12367]['centerLat'] = 44.27743183797234;	$territoriesArray[12367]['centerLong'] = 5.518375216626307;	$territoriesArray[12367]['mapDefaultZoom'] = 11;	
$territoriesArray[12394]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[12394]['name'] = "Mollans-sur-Ouvèze";	$territoriesArray[12394]['objective'] = "11";	$territoriesArray[12394]['centerLat'] = 44.23411657051591;	$territoriesArray[12394]['centerLong'] = 5.19440450893543;	$territoriesArray[12394]['mapDefaultZoom'] = 11;	
$territoriesArray[12764]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[12764]['name'] = "Montfroc";	$territoriesArray[12764]['objective'] = "4";	$territoriesArray[12764]['centerLat'] = 44.171377193140025;	$territoriesArray[12764]['centerLong'] = 5.644685019456035;	$territoriesArray[12764]['mapDefaultZoom'] = 11;	

$territoriesArray[13176]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[13176]['name'] = "Bénivay-Ollon";	$territoriesArray[13176]['objective'] = "4";	$territoriesArray[13176]['centerLat'] = 44.31081659724297;	$territoriesArray[13176]['centerLong'] = 5.187919106484672;	$territoriesArray[13176]['mapDefaultZoom'] = 11;	


$territoriesArray[13604]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[13604]['name'] = "Le Poët-Sigillat";	$territoriesArray[13604]['objective'] = "4";	$territoriesArray[13604]['centerLat'] = 44.36305998194293;	$territoriesArray[13604]['centerLong'] = 5.317694437754546;	$territoriesArray[13604]['mapDefaultZoom'] = 11;	
$territoriesArray[13685]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[13685]['name'] = "Les Pilles";	$territoriesArray[13685]['objective'] = "4";	$territoriesArray[13685]['centerLat'] = 44.37790816749534;	$territoriesArray[13685]['centerLong'] = 5.1993828020178645;	$territoriesArray[13685]['mapDefaultZoom'] = 11;	
$territoriesArray[13833]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[13833]['name'] = "Bouvières";	$territoriesArray[13833]['objective'] = "4";	$territoriesArray[13833]['centerLat'] = 44.52095390789802;	$territoriesArray[13833]['centerLong'] = 5.221127942966004;	$territoriesArray[13833]['mapDefaultZoom'] = 11;	
$territoriesArray[14120]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[14120]['name'] = "Sainte-Euphémie-sur-Ouvèze";	$territoriesArray[14120]['objective'] = "4";	$territoriesArray[14120]['centerLat'] = 44.30110326594081;	$territoriesArray[14120]['centerLong'] = 5.391477392122636;	$territoriesArray[14120]['mapDefaultZoom'] = 11;	
$territoriesArray[14652]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[14652]['name'] = "Rochebrune";	$territoriesArray[14652]['objective'] = "4";	$territoriesArray[14652]['centerLat'] = 44.326578447401324;	$territoriesArray[14652]['centerLong'] = 5.236905259457636;	$territoriesArray[14652]['mapDefaultZoom'] = 11;	
$territoriesArray[15436]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[15436]['name'] = "Châteauneuf-de-Bordette";	$territoriesArray[15436]['objective'] = "4";	$territoriesArray[15436]['centerLat'] = 44.336784371716114;	$territoriesArray[15436]['centerLong'] = 5.170880710109833;	$territoriesArray[15436]['mapDefaultZoom'] = 11;	
$territoriesArray[15578]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[15578]['name'] = "Bésignan";	$territoriesArray[15578]['objective'] = "4";	$territoriesArray[15578]['centerLat'] = 44.32017788708763;	$territoriesArray[15578]['centerLong'] = 5.321840208545314;	$territoriesArray[15578]['mapDefaultZoom'] = 11;	
$territoriesArray[15581]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[15581]['name'] = "Barret-de-Lioure";	$territoriesArray[15581]['objective'] = "4";	$territoriesArray[15581]['centerLat'] = 44.17544905822575;	$territoriesArray[15581]['centerLong'] = 5.5163012012929045;	$territoriesArray[15581]['mapDefaultZoom'] = 11;	
$territoriesArray[15750]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[15750]['name'] = "Lemps";	$territoriesArray[15750]['objective'] = "8";	$territoriesArray[15750]['centerLat'] = 44.35300633483751;	$territoriesArray[15750]['centerLong'] = 5.412788218515414;	$territoriesArray[15750]['mapDefaultZoom'] = 11;	
$territoriesArray[15770]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[15770]['name'] = "La Roche-sur-le-Buis";	$territoriesArray[15770]['objective'] = "3";	$territoriesArray[15770]['centerLat'] = 44.262805373664825;	$territoriesArray[15770]['centerLong'] = 5.3406442462230395;	$territoriesArray[15770]['mapDefaultZoom'] = 11;	
$territoriesArray[16168]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[16168]['name'] = "Valréas";	$territoriesArray[16168]['objective'] = "96";	$territoriesArray[16168]['centerLat'] = 44.376903957961865;	$territoriesArray[16168]['centerLong'] = 4.9935689192388395;	$territoriesArray[16168]['mapDefaultZoom'] = 11;	
$territoriesArray[17353]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[17353]['name'] = "Arpavon";	$territoriesArray[17353]['objective'] = "4";	$territoriesArray[17353]['centerLat'] = 44.37112414013355;	$territoriesArray[17353]['centerLong'] = 5.273184141098997;	$territoriesArray[17353]['mapDefaultZoom'] = 11;	
$territoriesArray[17368]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[17368]['name'] = "Saint-Léger-du-Ventoux";	$territoriesArray[17368]['objective'] = "4";	$territoriesArray[17368]['centerLat'] = 44.20601544033656;	$territoriesArray[17368]['centerLong'] = 5.270857416787305;	$territoriesArray[17368]['mapDefaultZoom'] = 11;	
$territoriesArray[18060]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[18060]['name'] = "La Charce";	$territoriesArray[18060]['objective'] = "4";	$territoriesArray[18060]['centerLat'] = 44.46640498903633;	$territoriesArray[18060]['centerLong'] = 5.444624951990775;	$territoriesArray[18060]['mapDefaultZoom'] = 11;	
$territoriesArray[18358]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[18358]['name'] = "Curnier";	$territoriesArray[18358]['objective'] = "4";	$territoriesArray[18358]['centerLat'] = 44.386995824216115;	$territoriesArray[18358]['centerLong'] = 5.234260979575991;	$territoriesArray[18358]['mapDefaultZoom'] = 11;	
$territoriesArray[18719]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[18719]['name'] = "Vesc";	$territoriesArray[18719]['objective'] = "3";	$territoriesArray[18719]['centerLat'] = 44.51015627537998;	$territoriesArray[18719]['centerLong'] = 5.157781547775842;	$territoriesArray[18719]['mapDefaultZoom'] = 11;	
$territoriesArray[18856]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[18856]['name'] = "Tulette";	$territoriesArray[18856]['objective'] = "21";	$territoriesArray[18856]['centerLat'] = 44.2841980362298;	$territoriesArray[18856]['centerLong'] = 4.937895688184863;	$territoriesArray[18856]['mapDefaultZoom'] = 11;	
$territoriesArray[19237]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[19237]['name'] = "Aurel";	$territoriesArray[19237]['objective'] = "4";	$territoriesArray[19237]['centerLat'] = 44.1382491611316;	$territoriesArray[19237]['centerLong'] = 5.39668722927606;	$territoriesArray[19237]['mapDefaultZoom'] = 11;	
$territoriesArray[19438]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[19438]['name'] = "Pelonne";	$territoriesArray[19438]['objective'] = "4";	$territoriesArray[19438]['centerLat'] = 44.38210492282665;	$territoriesArray[19438]['centerLong'] = 5.380963227945607;	$territoriesArray[19438]['mapDefaultZoom'] = 11;	
$territoriesArray[20230]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[20230]['name'] = "Montferrand-la-Fare";	$territoriesArray[20230]['objective'] = "4";	$territoriesArray[20230]['centerLat'] = 44.3422157976129;	$territoriesArray[20230]['centerLong'] = 5.449950617923839;	$territoriesArray[20230]['mapDefaultZoom'] = 11;	
$territoriesArray[20444]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[20444]['name'] = "Venterol";	$territoriesArray[20444]['objective'] = "4";	$territoriesArray[20444]['centerLat'] = 44.39776764556402;	$territoriesArray[20444]['centerLong'] = 5.098610443817861;	$territoriesArray[20444]['mapDefaultZoom'] = 11;	
$territoriesArray[21101]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21101]['name'] = "Salérans";	$territoriesArray[21101]['objective'] = "4";	$territoriesArray[21101]['centerLat'] = 44.242136571774125;	$territoriesArray[21101]['centerLong'] = 5.701182727738171;	$territoriesArray[21101]['mapDefaultZoom'] = 11;	
$territoriesArray[21237]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21237]['name'] = "Establet";	$territoriesArray[21237]['objective'] = "4";	$territoriesArray[21237]['centerLat'] = 44.50209635715835;	$territoriesArray[21237]['centerLong'] = 5.440052833305468;	$territoriesArray[21237]['mapDefaultZoom'] = 11;	
$territoriesArray[21288]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21288]['name'] = "Eyroles";	$territoriesArray[21288]['objective'] = "4";	$territoriesArray[21288]['centerLat'] = 44.41693137741343;	$territoriesArray[21288]['centerLong'] = 5.242364131658757;	$territoriesArray[21288]['mapDefaultZoom'] = 11;	
$territoriesArray[21382]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21382]['name'] = "Cornillac";	$territoriesArray[21382]['objective'] = "4";	$territoriesArray[21382]['centerLat'] = 44.43990333297924;	$territoriesArray[21382]['centerLong'] = 5.403727810014252;	$territoriesArray[21382]['mapDefaultZoom'] = 11;	
$territoriesArray[21444]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21444]['name'] = "Villedieu";	$territoriesArray[21444]['objective'] = "6";	$territoriesArray[21444]['centerLat'] = 44.28834272320376;	$territoriesArray[21444]['centerLong'] = 5.047528925377262;	$territoriesArray[21444]['mapDefaultZoom'] = 11;	
$territoriesArray[21789]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21789]['name'] = "Saint-Sauveur-Gouvernet";	$territoriesArray[21789]['objective'] = "4";	$territoriesArray[21789]['centerLat'] = 44.33040698707216;	$territoriesArray[21789]['centerLong'] = 5.376548988095141;	$territoriesArray[21789]['mapDefaultZoom'] = 11;	
$territoriesArray[21871]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[21871]['name'] = "Buisson";	$territoriesArray[21871]['objective'] = "3";	$territoriesArray[21871]['centerLat'] = 44.276280529797134;	$territoriesArray[21871]['centerLong'] = 5.001569522604691;	$territoriesArray[21871]['mapDefaultZoom'] = 11;	
$territoriesArray[22509]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[22509]['name'] = "Ferrassières";	$territoriesArray[22509]['objective'] = "4";	$territoriesArray[22509]['centerLat'] = 44.139422189274136;	$territoriesArray[22509]['centerLong'] = 5.490526284997034;	$territoriesArray[22509]['mapDefaultZoom'] = 11;	
$territoriesArray[22970]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[22970]['name'] = "Revest-du-Bion";	$territoriesArray[22970]['objective'] = "5";	$territoriesArray[22970]['centerLat'] = 44.09389410982129;	$territoriesArray[22970]['centerLong'] = 5.5406425425556;	$territoriesArray[22970]['mapDefaultZoom'] = 11;	
$territoriesArray[23012]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[23012]['name'] = "Sahune";	$territoriesArray[23012]['objective'] = "3";	$territoriesArray[23012]['centerLat'] = 44.4063487208782;	$territoriesArray[23012]['centerLong'] = 5.272083445591394;	$territoriesArray[23012]['mapDefaultZoom'] = 11;	
$territoriesArray[23695]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[23695]['name'] = "Aubres";	$territoriesArray[23695]['objective'] = "4";	$territoriesArray[23695]['centerLat'] = 44.38965937863197;	$territoriesArray[23695]['centerLong'] = 5.154778917617915;	$territoriesArray[23695]['mapDefaultZoom'] = 11;	
$territoriesArray[23960]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[23960]['name'] = "Mirabel-aux-Baronnies";	$territoriesArray[23960]['objective'] = "16";	$territoriesArray[23960]['centerLat'] = 44.31440327823722;	$territoriesArray[23960]['centerLong'] = 5.101762080087668;	$territoriesArray[23960]['mapDefaultZoom'] = 11;	
$territoriesArray[24233]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24233]['name'] = "Laborel";	$territoriesArray[24233]['objective'] = "4";	$territoriesArray[24233]['centerLat'] = 44.288819539311895;	$territoriesArray[24233]['centerLong'] = 5.591747699275512;	$territoriesArray[24233]['mapDefaultZoom'] = 11;	
$territoriesArray[24402]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24402]['name'] = "Cornillon-sur-l'Oule";	$territoriesArray[24402]['objective'] = "4";	$territoriesArray[24402]['centerLat'] = 44.456111024218536;	$territoriesArray[24402]['centerLong'] = 5.360045154028378;	$territoriesArray[24402]['mapDefaultZoom'] = 11;	
$territoriesArray[24618]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24618]['name'] = "Sorbiers";	$territoriesArray[24618]['objective'] = "4";	$territoriesArray[24618]['centerLat'] = 44.362267445465044;	$territoriesArray[24618]['centerLong'] = 5.573246134477543;	$territoriesArray[24618]['mapDefaultZoom'] = 11;	
$territoriesArray[24630]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24630]['name'] = "Saint-Ferréol-Trente-Pas";	$territoriesArray[24630]['objective'] = "3";	$territoriesArray[24630]['centerLat'] = 44.441475560236626;	$territoriesArray[24630]['centerLong'] = 5.223577392055133;	$territoriesArray[24630]['mapDefaultZoom'] = 11;	
$territoriesArray[24740]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24740]['name'] = "Visan";	$territoriesArray[24740]['objective'] = "20";	$territoriesArray[24740]['centerLat'] = 44.32689666679163;	$territoriesArray[24740]['centerLong'] = 4.944213210194718;	$territoriesArray[24740]['mapDefaultZoom'] = 11;	
$territoriesArray[24781]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24781]['name'] = "Mévouillon";	$territoriesArray[24781]['objective'] = "4";	$territoriesArray[24781]['centerLat'] = 44.23465157525539;	$territoriesArray[24781]['centerLong'] = 5.479219923143602;	$territoriesArray[24781]['mapDefaultZoom'] = 11;	
$territoriesArray[24920]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[24920]['name'] = "Sainte-Jalle";	$territoriesArray[24920]['objective'] = "3";	$territoriesArray[24920]['centerLat'] = 44.33356405245269;	$territoriesArray[24920]['centerLong'] = 5.278094728759174;	$territoriesArray[24920]['mapDefaultZoom'] = 11;	
$territoriesArray[25352]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[25352]['name'] = "Eygaliers";	$territoriesArray[25352]['objective'] = "4";	$territoriesArray[25352]['centerLat'] = 44.24021038415763;	$territoriesArray[25352]['centerLong'] = 5.284352090375353;	$territoriesArray[25352]['mapDefaultZoom'] = 11;	
$territoriesArray[25589]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[25589]['name'] = "Chaudebonne";	$territoriesArray[25589]['objective'] = "4";	$territoriesArray[25589]['centerLat'] = 44.4749073323465;	$territoriesArray[25589]['centerLong'] = 5.2386811215351194;	$territoriesArray[25589]['mapDefaultZoom'] = 11;	
$territoriesArray[25641]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[25641]['name'] = "Montbrun-les-Bains";	$territoriesArray[25641]['objective'] = "5";	$territoriesArray[25641]['centerLat'] = 44.18765595786629;	$territoriesArray[25641]['centerLong'] = 5.434100817212255;	$territoriesArray[25641]['mapDefaultZoom'] = 11;	
$territoriesArray[25676]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[25676]['name'] = "Sainte-Colombe";	$territoriesArray[25676]['objective'] = "4";	$territoriesArray[25676]['centerLat'] = 44.28915416321183;	$territoriesArray[25676]['centerLong'] = 5.66826979138902;	$territoriesArray[25676]['mapDefaultZoom'] = 11;	
$territoriesArray[25719]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[25719]['name'] = "Roussieux";	$territoriesArray[25719]['objective'] = "4";	$territoriesArray[25719]['centerLat'] = 44.331797096568046;	$territoriesArray[25719]['centerLong'] = 5.478811566488511;	$territoriesArray[25719]['mapDefaultZoom'] = 11;	
$territoriesArray[26104]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26104]['name'] = "Savoillan";	$territoriesArray[26104]['objective'] = "4";	$territoriesArray[26104]['centerLat'] = 44.17489174441679;	$territoriesArray[26104]['centerLong'] = 5.372086622280432;	$territoriesArray[26104]['mapDefaultZoom'] = 11;	
$territoriesArray[26414]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26414]['name'] = "Propiac";	$territoriesArray[26414]['objective'] = "4";	$territoriesArray[26414]['centerLat'] = 44.27656847370454;	$territoriesArray[26414]['centerLong'] = 5.20884421397371;	$territoriesArray[26414]['mapDefaultZoom'] = 11;	
$territoriesArray[26485]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26485]['name'] = "Izon-la-Bruisse";	$territoriesArray[26485]['objective'] = "4";	$territoriesArray[26485]['centerLat'] = 44.26276463062828;	$territoriesArray[26485]['centerLong'] = 5.60242919178797;	$territoriesArray[26485]['mapDefaultZoom'] = 11;	
$territoriesArray[26487]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26487]['name'] = "Moydans";	$territoriesArray[26487]['objective'] = "4";	$territoriesArray[26487]['centerLat'] = 44.40970348908045;	$territoriesArray[26487]['centerLong'] = 5.50937993207922;	$territoriesArray[26487]['mapDefaultZoom'] = 11;	
$territoriesArray[26512]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26512]['name'] = "Reilhanette";	$territoriesArray[26512]['objective'] = "4";	$territoriesArray[26512]['centerLat'] = 44.17128141582504;	$territoriesArray[26512]['centerLong'] = 5.404970045278045;	$territoriesArray[26512]['mapDefaultZoom'] = 11;	
$territoriesArray[26556]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26556]['name'] = "Vercoiran";	$territoriesArray[26556]['objective'] = "4";	$territoriesArray[26556]['centerLat'] = 44.293695974735876;	$territoriesArray[26556]['centerLong'] = 5.3500413769578765;	$territoriesArray[26556]['mapDefaultZoom'] = 11;	
$territoriesArray[26663]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26663]['name'] = "Rosans";	$territoriesArray[26663]['objective'] = "5";	$territoriesArray[26663]['centerLat'] = 44.40059087510512;	$territoriesArray[26663]['centerLong'] = 5.4613198452762655;	$territoriesArray[26663]['mapDefaultZoom'] = 11;	
$territoriesArray[26726]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26726]['name'] = "Le Poët-en-Percip";	$territoriesArray[26726]['objective'] = "4";	$territoriesArray[26726]['centerLat'] = 44.24856832791823;	$territoriesArray[26726]['centerLong'] = 5.394896477082771;	$territoriesArray[26726]['mapDefaultZoom'] = 11;	
$territoriesArray[26805]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26805]['name'] = "Aulan";	$territoriesArray[26805]['objective'] = "4";	$territoriesArray[26805]['centerLat'] = 44.22987708053183;	$territoriesArray[26805]['centerLong'] = 5.421916831062311;	$territoriesArray[26805]['mapDefaultZoom'] = 11;	
$territoriesArray[26824]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[26824]['name'] = "Bellecombe-Tarendol";	$territoriesArray[26824]['objective'] = "4";	$territoriesArray[26824]['centerLat'] = 44.35645351491325;	$territoriesArray[26824]['centerLong'] = 5.355384978620225;	$territoriesArray[26824]['mapDefaultZoom'] = 11;	
$territoriesArray[27581]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[27581]['name'] = "Saint-May";	$territoriesArray[27581]['objective'] = "4";	$territoriesArray[27581]['centerLat'] = 44.42436577238153;	$territoriesArray[27581]['centerLong'] = 5.329736211143973;	$territoriesArray[27581]['mapDefaultZoom'] = 11;	
$territoriesArray[28169]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[28169]['name'] = "Saint-Pantaléon-les-Vignes";	$territoriesArray[28169]['objective'] = "4";	$territoriesArray[28169]['centerLat'] = 44.395797466957895;	$territoriesArray[28169]['centerLong'] = 5.042149612837755;	$territoriesArray[28169]['mapDefaultZoom'] = 11;	
$territoriesArray[28250]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[28250]['name'] = "Montréal-les-Sources";	$territoriesArray[28250]['objective'] = "4";	$territoriesArray[28250]['centerLat'] = 44.400616457045125;	$territoriesArray[28250]['centerLong'] = 5.306829607552041;	$territoriesArray[28250]['mapDefaultZoom'] = 11;	
$territoriesArray[28512]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[28512]['name'] = "La Rochette-du-Buis";	$territoriesArray[28512]['objective'] = "4";	$territoriesArray[28512]['centerLat'] = 44.261093647681754;	$territoriesArray[28512]['centerLong'] = 5.421073631397078;	$territoriesArray[28512]['mapDefaultZoom'] = 11;	
$territoriesArray[28702]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[28702]['name'] = "La Motte-Chalancon";	$territoriesArray[28702]['objective'] = "4";	$territoriesArray[28702]['centerLat'] = 44.49546665067502;	$territoriesArray[28702]['centerLong'] = 5.384695918692965;	$territoriesArray[28702]['mapDefaultZoom'] = 11;	
$territoriesArray[29181]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[29181]['name'] = "Beauvoisin";	$territoriesArray[29181]['objective'] = "4";	$territoriesArray[29181]['centerLat'] = 44.30089197007326;	$territoriesArray[29181]['centerLong'] = 5.21289475757095;	$territoriesArray[29181]['mapDefaultZoom'] = 11;	
$territoriesArray[29366]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[29366]['name'] = "Rémuzat";	$territoriesArray[29366]['objective'] = "3";	$territoriesArray[29366]['centerLat'] = 44.40366034940999;	$territoriesArray[29366]['centerLong'] = 5.356249585168668;	$territoriesArray[29366]['mapDefaultZoom'] = 11;	
$territoriesArray[29385]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[29385]['name'] = "Vinsobres";	$territoriesArray[29385]['objective'] = "11";	$territoriesArray[29385]['centerLat'] = 44.33557106172709;	$territoriesArray[29385]['centerLong'] = 5.05043366572269;	$territoriesArray[29385]['mapDefaultZoom'] = 11;	
$territoriesArray[29977]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[29977]['name'] = "Barret-sur-Méouge";	$territoriesArray[29977]['objective'] = "4";	$territoriesArray[29977]['centerLat'] = 44.26487335819099;	$territoriesArray[29977]['centerLong'] = 5.728370677195471;	$territoriesArray[29977]['mapDefaultZoom'] = 11;	
$territoriesArray[30771]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[30771]['name'] = "Eygalayes";	$territoriesArray[30771]['objective'] = "4";	$territoriesArray[30771]['centerLat'] = 44.221797518078624;	$territoriesArray[30771]['centerLong'] = 5.5942295879932;	$territoriesArray[30771]['mapDefaultZoom'] = 11;	
$territoriesArray[31011]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[31011]['name'] = "Rottier";	$territoriesArray[31011]['objective'] = "4";	$territoriesArray[31011]['centerLat'] = 44.478788253764726;	$territoriesArray[31011]['centerLong'] = 5.416705553888104;	$territoriesArray[31011]['mapDefaultZoom'] = 11;	
$territoriesArray[31281]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[31281]['name'] = "Villefranche-le-Château";	$territoriesArray[31281]['objective'] = "4";	$territoriesArray[31281]['centerLat'] = 44.21532567446516;	$territoriesArray[31281]['centerLong'] = 5.5081157151997395;	$territoriesArray[31281]['mapDefaultZoom'] = 11;	
$territoriesArray[32200]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[32200]['name'] = "Condorcet";	$territoriesArray[32200]['objective'] = "5";	$territoriesArray[32200]['centerLat'] = 44.41518816058821;	$territoriesArray[32200]['centerLong'] = 5.178442363150043;	$territoriesArray[32200]['mapDefaultZoom'] = 11;	
$territoriesArray[32542]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[32542]['name'] = "Buis-les-Baronnies";	$territoriesArray[32542]['objective'] = "24";	$territoriesArray[32542]['centerLat'] = 44.27626125622169;	$territoriesArray[32542]['centerLong'] = 5.271190462925369;	$territoriesArray[32542]['mapDefaultZoom'] = 11;	
$territoriesArray[32592]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[32592]['name'] = "Plaisians";	$territoriesArray[32592]['objective'] = "4";	$territoriesArray[32592]['centerLat'] = 44.22555074116832;	$territoriesArray[32592]['centerLong'] = 5.337063115073038;	$territoriesArray[32592]['mapDefaultZoom'] = 11;	
$territoriesArray[32607]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[32607]['name'] = "Montguers";	$territoriesArray[32607]['objective'] = "4";	$territoriesArray[32607]['centerLat'] = 44.302055093097614;	$territoriesArray[32607]['centerLong'] = 5.472134922071549;	$territoriesArray[32607]['mapDefaultZoom'] = 11;	
$territoriesArray[33363]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[33363]['name'] = "Montaulieu";	$territoriesArray[33363]['objective'] = "4";	$territoriesArray[33363]['centerLat'] = 44.35781550446364;	$territoriesArray[33363]['centerLong'] = 5.217631319455239;	$territoriesArray[33363]['mapDefaultZoom'] = 11;	
$territoriesArray[33608]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[33608]['name'] = "Saint-Maurice-sur-Eygues";	$territoriesArray[33608]['objective'] = "8";	$territoriesArray[33608]['centerLat'] = 44.303014362200535;	$territoriesArray[33608]['centerLong'] = 5.005604712119984;	$territoriesArray[33608]['mapDefaultZoom'] = 11;	
$territoriesArray[34362]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[34362]['name'] = "Piégon";	$territoriesArray[34362]['objective'] = "3";	$territoriesArray[34362]['centerLat'] = 44.299118858717584;	$territoriesArray[34362]['centerLong'] = 5.124974704997768;	$territoriesArray[34362]['mapDefaultZoom'] = 11;	
$territoriesArray[34729]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[34729]['name'] = "Chauvac-Laux-Montaux";	$territoriesArray[34729]['objective'] = "4";	$territoriesArray[34729]['centerLat'] = 44.322231564495276;	$territoriesArray[34729]['centerLong'] = 5.5342585243452485;	$territoriesArray[34729]['mapDefaultZoom'] = 11;	
$territoriesArray[35042]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[35042]['name'] = "Pommerol";	$territoriesArray[35042]['objective'] = "4";	$territoriesArray[35042]['centerLat'] = 44.43745922272628;	$territoriesArray[35042]['centerLong'] = 5.4649037933216995;	$territoriesArray[35042]['mapDefaultZoom'] = 11;	
$territoriesArray[35266]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[35266]['name'] = "Teyssières";	$territoriesArray[35266]['objective'] = "4";	$territoriesArray[35266]['centerLat'] = 44.45764815935349;	$territoriesArray[35266]['centerLong'] = 5.142953161005764;	$territoriesArray[35266]['mapDefaultZoom'] = 11;	
$territoriesArray[35466]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[35466]['name'] = "Valouse";	$territoriesArray[35466]['objective'] = "4";	$territoriesArray[35466]['centerLat'] = 44.45950537510513;	$territoriesArray[35466]['centerLong'] = 5.1912878877559985;	$territoriesArray[35466]['mapDefaultZoom'] = 11;	
$territoriesArray[35832]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[35832]['name'] = "Rousset-les-Vignes";	$territoriesArray[35832]['objective'] = "3";	$territoriesArray[35832]['centerLat'] = 44.42323071304699;	$territoriesArray[35832]['centerLong'] = 5.068014957342572;	$territoriesArray[35832]['mapDefaultZoom'] = 11;	
$territoriesArray[35991]['campaignTitle'] = "Devenez un rossignol, la suite !";	$territoriesArray[35991]['name'] = "Nyons";	$territoriesArray[35991]['objective'] = "70";	$territoriesArray[35991]['centerLat'] = 44.356544262565;	$territoriesArray[35991]['centerLong'] = 5.128999901175242;	$territoriesArray[35991]['mapDefaultZoom'] = 11;	


?>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
    <style>
      .map {
        height: 400px;
        width: 600px;
      }
      
      
      #Logo {
        width: 230px;
        height: 60px;
        display: block;
        position: absolute;
        top: 0px;
        left: 0px;
        text-align:center;
        padding: 10px;       
        background-color: #00D28C;
      }
      
      #ProgressMonitoring {
        height: 400px;
        width: 300px;
        display: block;
        position: absolute;
        top: 80px;
        right: 30px;
      }
      
      #myProgress {
        width: 100%;
        background-color: grey;
      }
      
      #MainButton {
        position: relative;
        top: 20px;
        text-align: center;
      }
      
      
      #MoreInformation {
        position: relative;
        top: 80px;
        padding: 20px;        
        background-color: #00D28C;
        
      }

      #myBar {
        width: 0%;
        height: 30px;
        background-color: #04AA6D;
        text-align: center; /* To center it horizontally (if you want) */
        line-height: 30px; /* To center it vertically */
        color: white;
      }
      
      .button {
        background-color: #f045ab;
        border: none;
        color: white;
        padding: 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        margin: 4px 2px;
        border-radius: 28px;
      }
      .button:hover {
         cursor:pointer;
      }
      
      .campaignIndicator {
        display: flex;
        align-items: center;
        justify-content: center
      }
      
      
      #campaignDatesRangeBlock {
        color: #7A7A7C;
        text-align:center;
        position: relative;
        top: 100px;
      }
      
      .rangeEnd {
        font-style: italic;
      }
      .rangeValue {
        font-weight: normal;
      }
    </style>
    <!--
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.12.0/css/ol.css" type="text/css">
    <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.12.0/build/ol.js"></script>
    -->
    
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    
    
    <script src="https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.js"></script>
    
    <title>
<?php
if(is_null($territoriesArray[$territory_id]['campaignTitle']) || $territoriesArray[$territory_id]['campaignTitle'] == "") {
  echo "Campagne sur le territoire \"".$territoriesArray[$territory_id]['name']."\"";
} else {
  echo $territoriesArray[$territory_id]['campaignTitle'];
}
?>
</title>
  </head>
  <body onLoad="progressMove()">
<?php
if(is_null($territoriesArray[$territory_id]['campaignTitle']) || $territoriesArray[$territory_id]['campaignTitle'] == "") {
?>
  <h2><?="Campagne sur le territoire \"".$territoriesArray[$territory_id]['name']."\""?></h2>
<?php
} else {
?>
    <h1 style="text-align:center"><?=$territoriesArray[$territory_id]['campaignTitle']?></h1>

        <?php
        // TODO retirer le caractètre "magique" du 528
        if ( !is_null($territoriesArray[528]['children']) && in_array($territory_id, explode(',', $territoriesArray[528]['children'])) ) {
        ?>
            <p style="font-style:italic"><a href="campaign.php?territory_id=528">Revenir sur la page de toute la campagne <?=$territoriesArray[$territory_id]['campaignTitle']?></a></p>
            <h3 style="font-style:italic">Campagne sur la commune <br/>"<?=$territoriesArray[$territory_id]['name']?>"</h3>

    <div id="MainButton">
      <a href="data/ListeCovoituragesReguliers_<?=$territory_id?>.docx"><button class="button">Télécharger la liste des covoiturages de cette commune</button></a>
    </div>

        

        <?php
        } else {
        ?>
            <h3 style="font-style:italic">Campagne sur le territoire <br/>"<?=$territoriesArray[$territory_id]['name']?>"</h3>
        <?php
        }
        ?>


<?php
}
?>


<!--    
    <div id="Logo">
      <img src="https://www.mobicoop.fr/images/pages/home/MOBICOOP_LOGO-V1 Blanc.svg" alt="Mobicoop" width="210" height="50">
    </div>
-->    

  <div id="map" class="map"></div>
    
    <div id="ProgressMonitoring">
      <b><span id="driversCount">0</span> conducteurs recrutés</b>
 <?php
 if ($territoriesArray[$territory_id]['objective'] !== NULL) {
 ?>
      <div id="myProgress">
        <div id="myBar">0%</div>
      </div>
        sur <?=$territoriesArray[$territory_id]['objective']?> conducteurs pour masse critique locale!
 <?php
 }
 if(!is_null($territoriesArray[$territory_id]['endDate']) && $territoriesArray[$territory_id]['endDate'] != "") {
 
  function dateToFrench($date, $format)   {
      $english_shortdays = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
      $french_shortdays = array('Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim');
      $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
      $french_days = array('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche');
      $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
      $french_months = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
      return str_replace($english_months, $french_months, str_replace($english_shortdays, $french_shortdays, str_replace($english_days, $french_days, date($format, date_timestamp_get($date) ) ) ) );
  }
   
  //Create a date object out of a string (e.g. from a database):
  $endDate = date_create_from_format('Y-m-d', $territoriesArray[$territory_id]['endDate']);

  //Create a date object out of today's date:
  $nowDate = date_create_from_format('Y-m-d', date('Y-m-d'));

  //Create a comparison of the two dates and store it in an array:
  $dateDiff = (array) date_diff($endDate, $nowDate);
  
  $daysLeft = ($dateDiff['days']>0 ? $dateDiff['days'] : 0);
  
 ?>
        <div>
          <p>
            <div class="campaignIndicator"><img src="clock_icon.png"><span><?=$daysLeft?></span></div>
            <div style="text-align:center">jours restants</div>
          </p>
        </div>
        
        <div id="campaignDatesRangeBlock">
          <?php
          if(!is_null($territoriesArray[$territory_id]['startDate']) && $territoriesArray[$territory_id]['startDate'] != "") {
            $startDate = date_create_from_format('Y-m-d', $territoriesArray[$territory_id]['startDate']);
          ?>
          <span class="rangeEnd">Début:</span> <span class="rangeValue"><?=dateToFrench($startDate, 'D j F')?></span><br />
          <?php
          }
          ?>
          <span class="rangeEnd">Fin:</span> <span class="rangeValue"><?=dateToFrench($endDate, 'D j F')?></span>
        </div>
<?php
}
?>
        
    </div>

    <div id="MainButton">
      <a href="https://www.mobicoop.fr/utilisateur/inscription"><button class="button">S'inscrire et publier son trajet régulier</button></a> <br />
      <?=$territoriesArray[$territory_id]['campaignTitle']?>
    </div>


<?php
if (!is_null($territoriesArray[$territory_id]['pdfMoreInformation']) && $territoriesArray[$territory_id]['pdfMoreInformation'] != "") {
?>
    <div id="MoreInformation">
      <h3>En savoir plus</h3>
      <div style="text-align: center;">
      <canvas id="pdfMoreInformation"></canvas>
      </div>
      <script type="text/javascript">
            // If absolute URL from the remote server is provided, configure the CORS
      // header on that server.
      var url = 'http://www.campagne-masse-critique-covoiturage.be/<?=$territoriesArray[$territory_id]['pdfMoreInformation']?>';

      // Loaded via <script> tag, create shortcut to access PDF.js exports.
      var pdfjsLib = window['pdfjs-dist/build/pdf'];

      // The workerSrc property shall be specified.
      pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://unpkg.com/pdfjs-dist@2.13.216/build/pdf.worker.js';

      // Asynchronous download of PDF
      var loadingTask = pdfjsLib.getDocument(url);
      loadingTask.promise.then(function(pdf) {
        console.log('PDF loaded');
        
        // Fetch the first page
        var pageNumber = 1;
        pdf.getPage(pageNumber).then(function(page) {
          console.log('Page loaded');
          
          var scale = 1.5;
          var viewport = page.getViewport({scale: scale});

          // Prepare canvas using PDF page dimensions
          var canvas = document.getElementById('pdfMoreInformation');
          var context = canvas.getContext('2d');
          canvas.height = viewport.height;
          canvas.width = viewport.width;

          // Render PDF page into canvas context
          var renderContext = {
            canvasContext: context,
            viewport: viewport
          };
          var renderTask = page.render(renderContext);
          renderTask.promise.then(function () {
            console.log('Page rendered');
          });
        });
      }, function (reason) {
        // PDF loading error
        console.error(reason);
      });
      </script>
    </div>
<?php
}
if (!is_null($territoriesArray[$territory_id]['children']) && $territoriesArray[$territory_id]['children'] != "") {
?>
<h3 style="font-style:italic">Suivre chacune des communes liées à cette campagne <?=$territoriesArray[$territory_id]['campaignTitle']?></h3>
<ul>
<?php  
    foreach(explode(',',$territoriesArray[$territory_id]['children']) as $childId) {
?>
        <li><a href="campaign.php?territory_id=<?=$childId?>"><?=$territoriesArray[$childId]['name']?></a></li>
<?php
    }
?>
</ul>


<?php
}
?> 
    <script src="data/territory_<?=$territory_id?>.js" type="text/javascript"></script>
    
    <script type="text/javascript">
  var map = L.map('map').setView([<?=$territoriesArray[$territory_id]['centerLat']?>, <?=$territoriesArray[$territory_id]['centerLong']?>], <?=$territoriesArray[$territory_id]['mapDefaultZoom']?>);
  
  var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png?{foo}', {foo: 'bar', attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}).addTo(map);

  
  
      function onEachFeature(feature, layer) {
        var popupContent = '';

        if (feature.properties && feature.properties.popupContent) {
          popupContent = feature.properties.popupContent;
        }

        layer.bindPopup(popupContent);
      }
      
      var carpoolAdsLayer = L.geoJSON([boundaries, carpoolAds], {

        style: function (feature) {
          return feature.properties && feature.properties.style;
        },
        onEachFeature: onEachFeature
      }).addTo(map);
    </script>

    <script type="text/javascript">
      var driversCountElem = document.getElementById("driversCount");
      
      var userArray = [];
      for (featureIndex = 0; featureIndex < carpoolAds.features.length; featureIndex++) {
        if (userArray.indexOf(carpoolAds.features[featureIndex].userID) == -1) {
          userArray.push(carpoolAds.features[featureIndex].userID);
        }        
      }
      
      driversCountElem.innerHTML = userArray.length;
      
      var progress = Math.round((userArray.length / <?=$territoriesArray[$territory_id]['objective']?>)*100,0);
      
      var i = 0;
      function progressMove() {
        if (i == 0) {
          i = 1;
          var elem = document.getElementById("myBar");
          var width = 0;
          var id = setInterval(frame, 10);
          function frame() {
            if (width >= progress) {
              clearInterval(id);
              i = 0;
            } else {
              width++;
              elem.style.width = width + "%";
              elem.innerHTML = width + "%";
            }
          }
        }
      } 
    </script>
    
  </body>
</html>
