<?php
/**
 * Copyright (c) 2022, MOBICOOP. All rights reserved.
 ***************************
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <gnu.org/licenses>.
 ***************************
 *    Licence MOBICOOP described in the file
 *    LICENSE
 **************************/

/*
    Configuration file template
*/

/* Mobicoop Platform API */
$API_BASE_URI = "localhost";
$API_USERNAME = "username";
$API_PASSWORD = "password";


/* Mariadb own persistence database */
$DB_SERVER = "localhost";
$DB_PORT = 3306;
$DB_USERNAME = "dbusername";
$DB_PASSWORD = "dbpassword";
$DB_DBNAME = "enough";


/* To blur the eaxct positions of the proposals collected from the API: set NULL to keep exact position*/
$BLUR_POSITION_ROUND_DECIMALS = 2; // default value used in Mobicoop Scope


?>
