<?php
/* **************************************************************************************

	Produce A GeoJSON file compatible with OpenLayers

************************************************************************************** */


/* Load configuration */
require_once('config.dist.php');
include('config.local.php');

function blurCoordinate($coordinate) {
    global $BLUR_POSITION_ROUND_DECIMALS;
    return ( is_null($BLUR_POSITION_ROUND_DECIMALS) ? $coordinate : round($coordinate, $BLUR_POSITION_ROUND_DECIMALS) );
}


/*

    CLI Parameters management

*/


if ($argc !=2 ) {
    echo "!!Wrong arguments' count\nsyntax: php prepare-data.php TERRITORY_ID\n";
    exit(1);
} else {
    $territory_id = $argv[1];
}

$mysqli = new mysqli($DB_REMOTE_SERVER, $DB_REMOTE_USERNAME, $DB_REMOTE_PASSWORD, $DB_REMOTE_DBNAME, $DB_REMOTE_PORT);


/*
    Get the territory details
*/
if ( $result =  $mysqli->query("SELECT name, admin_level FROM territory WHERE id=$territory_id;") ) {
	while($obj = $result->fetch_object()) {
	    $territory_fullname = $obj->name;
	    $territory_shortname = $obj->name; // for latter usage, when deparment code will be added to commune name
	    $territory_adminlevel = $obj->admin_level;
	}
}

echo "$territory_id => $territory_shortname (admin level $territory_adminlevel)\n";

/*
	Build the GeoJSON core content
*/


/* carpoolAds  */

$query = <<<EOT
select PASSWORD(p.user_id) as Conducteur, 
    p.id as AnnonceId,
    ad.address_locality as Depart,
    ROUND(ad.latitude, $BLUR_POSITION_ROUND_DECIMALS) as Depart_latitude,
    ROUND(ad.longitude, $BLUR_POSITION_ROUND_DECIMALS) as Depart_longitude,
    aa.address_locality as Arrivee,
    ROUND(aa.latitude, $BLUR_POSITION_ROUND_DECIMALS) as Arrivee_latitude,
    ROUND(aa.longitude, $BLUR_POSITION_ROUND_DECIMALS) as Arrivee_longitude,
    CONCAT(IF(c.mon_check=1, 'lundi/', ''), IF(c.tue_check=1, 'mardi/', ''), IF(c.wed_check=1, 'mercredi/', ''), IF(c.thu_check=1, 'jeudi/', ''), IF(c.fri_check=1, 'vendredi/', ''), IF(c.sat_check=1, 'samedi/', ''), IF(c.sun_check=1, 'dimanche/', '')) as Date,
    CASE p.type 
    	WHEN 1 THEN 'Aller simple'
    	WHEN 2 THEN 'Aller d''un aller-retour'
    END as TypeTrajet,
    CASE 
        WHEN c.driver=1 and c.passenger=1 THEN 'Peu importe'
        WHEN (c.driver=0 or c.driver is null) and c.passenger=1 THEN 'Passager'
        WHEN c.driver=1 and (c.passenger=0 or c.passenger is null) THEN 'Conducteur'
        ELSE 'aberrant'
    END as TypeAnnonce
from proposal p
    inner join criteria c on c.id = p.criteria_id
    inner join waypoint wd on (wd.proposal_id = p.id and wd.position=0)
    inner join waypoint wa on (wa.proposal_id = p.id and wa.destination=1)
    inner join address ad on ad.id = wd.address_id
    inner join address aa on aa.id = wa.address_id
	left outer join direction dd on dd.id = c.direction_driver_id
	left outer join direction pd on pd.id = c.direction_passenger_id
where p.private=0 and (p.dynamic!=1 or p.dynamic is null) and p.type between 1 and 2 and c.frequency=2 and c.driver=1 and c.from_date <= NOW() and c.to_date > NOW() and (wd.address_id in (SELECT address_id FROM address_territory atd WHERE atd.territory_id=$territory_id) OR wa.address_id in (SELECT address_id FROM address_territory ata WHERE ata.territory_id=$territory_id));
EOT;


$featuresArray = array();
if ( $result =  $mysqli->query($query) ) {
	while($obj = $result->fetch_object()) {
	    $featuresArray[] = <<<EOT

        {
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [$obj->Depart_longitude, $obj->Depart_latitude],
                    [$obj->Arrivee_longitude, $obj->Arrivee_latitude]
                ]
            },
            "properties": {
                "popupContent": "$obj->Date<br />$obj->TypeTrajet<br />$obj->TypeAnnonce"
            },
            "id": $obj->AnnonceId,
            "userID" : "$obj->Conducteur"
        }
EOT;
	}
}
$result->close(); 
$geojson = implode(",", $featuresArray);


$geojsonHeader = <<<EOT
var carpoolAds = {
    "type": "FeatureCollection",
    "features": [

EOT;
$geojsonFooter = <<<EOT

    ]
};

EOT;

/* boundaries  */
$query = "SELECT id, name, st_asgeojson(geo_json_detail) as geoJSON FROM `territory` WHERE id=$territory_id;";


$boundariesGeoJson = "";
if ( $result =  $mysqli->query($query) ) {
	while($obj = $result->fetch_object()) {
	    $boundariesGeoJson = <<<EOT

var boundaries = {
    "type": "Feature",
    "properties": {
        "popupContent": "$obj->name",
        "style": {
            weight: 2,
            color: "#900",
            opacity: 1,
            fillColor: "#B0DE5C",
            fillOpacity: 0.0
        }
    },
    "geometry": $obj->geoJSON
};
EOT;
	}
}



/*
	Output the GeoJSON to a temporary file
*/


$filename = "territory_$territory_id.js";
$local_filepath = $LOCAL_TEMP_FOLDER . $filename;

file_put_contents($local_filepath, $geojsonHeader . $geojson . $geojsonFooter . $boundariesGeoJson);


/*
	Create list of carpoolAds export file
*/
if ($territory_adminlevel == 4) {
    $query = <<<EOT
select
    CASE ad.address_locality
        WHEN '$territory_shortname' THEN aa.address_locality
        ELSE ad.address_locality
    END as AutreCommune, 
    CASE ad.address_locality
        WHEN '$territory_shortname' THEN '1.Depuis filtre'
        ELSE '2.Vers filtre'
    END as Sens,
    p.id as AnnonceId,
    ad.address_locality as Depart,
    aa.address_locality as Arrivee,
    IF(c.mon_check=1, DATE_FORMAT(c.mon_time, '%k:%i') , '') as Lundi,
    IF(c.tue_check=1, DATE_FORMAT(c.tue_time, '%k:%i') , '') as Mardi,
    IF(c.wed_check=1, DATE_FORMAT(c.wed_time, '%k:%i') , '') as Mercredi,
    IF(c.thu_check=1, DATE_FORMAT(c.thu_time, '%k:%i') , '') as Jeudi,
    IF(c.fri_check=1, DATE_FORMAT(c.fri_time, '%k:%i') , '') as Vendredi,
    IF(c.sat_check=1, DATE_FORMAT(c.sat_time, '%k:%i') , '') as Samedi,
    IF(c.sun_check=1, DATE_FORMAT(c.sun_time, '%k:%i') , '') as Dimanche,
    CASE 
        WHEN c.driver=1 and c.passenger=1 THEN 'Peu importe'
        WHEN (c.driver=0 or c.driver is null) and c.passenger=1 THEN 'Passager'
        WHEN c.driver=1 and (c.passenger=0 or c.passenger is null) THEN 'Conducteur'
        ELSE 'aberrant'
    END as TypeAnnonce,
    CONCAT(u.given_name, ' ', substring(u.family_name, 1,1)) as Conducteur
from proposal p
    inner join user u on u.id = p.user_id
    inner join criteria c on c.id = p.criteria_id
    inner join waypoint wd on (wd.proposal_id = p.id and wd.position=0)
    inner join waypoint wa on (wa.proposal_id = p.id and wa.destination=1)
    inner join address ad on ad.id = wd.address_id
    inner join address aa on aa.id = wa.address_id
	left outer join direction dd on dd.id = c.direction_driver_id
	left outer join direction pd on pd.id = c.direction_passenger_id
where p.private=0 and (p.dynamic!=1 or p.dynamic is null) and c.frequency=2 and c.driver=1 and c.from_date <= NOW() and c.to_date > NOW() and (wd.address_id in (SELECT address_id FROM address_territory atd WHERE atd.territory_id=$territory_id) OR wa.address_id in (SELECT address_id FROM address_territory ata WHERE ata.territory_id=$territory_id))
ORDER by AutreCommune, Sens, c.mon_time, c.tue_time, c.wed_time, c.thu_time, c.fri_time, c.sat_time, c.sun_time;
EOT;


    $carpoolExportRowsArray = array();
    $previousAutreCommune = "";
    if ( $result =  $mysqli->query($query) ) {
        while($obj = $result->fetch_object()) {
            if($obj->AutreCommune != $previousAutreCommune) {
                $previousAutreCommune = $obj->AutreCommune;
                $carpoolExportRowsArray[$previousAutreCommune] = array();
                $carpoolExportRowsArray[$previousAutreCommune][1] = array();
                $carpoolExportRowsArray[$previousAutreCommune][2] = array();
            }
            $carpoolExportRowsArray[$previousAutreCommune][substr($obj->Sens,0,1)][] = array(
                $obj->Lundi,
                $obj->Mardi,
                $obj->Mercredi,
                $obj->Jeudi,
                $obj->Vendredi,
                $obj->Samedi,
                $obj->Dimanche,
                $obj->Conducteur
            );
        }
    }
    $result->close(); 
    
        
    include_once 'phpword/Sample_Header.php';

    // Template processor instance creation
    echo date('H:i:s'), ' Creating new TemplateProcessor instance...', EOL;
    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('phpword/resources/TownCarpoolList_template.docx');

    // Variables on different parts of document
    setlocale(LC_TIME, 'fr_FR');
    date_default_timezone_set('Europe/Paris');
//    $templateProcessor->setValue('printDate', utf8_encode(strftime("%d/%m/%Y")));            // On section/content
    $templateProcessor->setValue('printDate', strftime("%d/%m/%Y"));            // On section/content
    $templateProcessor->setValue('nomCommune', $territory_shortname);

    // First : let's repeat the blocks
    $templateProcessor->cloneBlock('autreCommuneBlock', count($carpoolExportRowsArray), true, true);
    
    $autreCommuneIndex = 1;
    foreach($carpoolExportRowsArray as $carpoolExportAutreCommuneArray) {
        
        $templateProcessor->setValue('autreCommune#'.$autreCommuneIndex, array_keys($carpoolExportRowsArray)[$autreCommuneIndex-1]);
        
        $templateProcessor->setValue("mo1", "mo1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("tu1", "tu1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("we1", "we1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("th1", "th1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("fr1", "fr1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("sa1", "sa1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("su1", "su1_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("driverAlias1", "driverAlias1_".$autreCommuneIndex, $autreCommuneIndex);

        $templateProcessor->setValue("mo2", "mo2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("tu2", "tu2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("we2", "we2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("th2", "th2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("fr2", "fr2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("sa2", "sa2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("su2", "su2_".$autreCommuneIndex, $autreCommuneIndex);
        $templateProcessor->setValue("driverAlias2", "driverAlias2_".$autreCommuneIndex, $autreCommuneIndex);
        
        $autreCommuneIndex++;
    }

    // Secondly : let's fill the tables
    $autreCommuneIndex = 1;
    foreach($carpoolExportRowsArray as $carpoolExportAutreCommuneArray) {
        $templateProcessor->cloneRow('mo1#'.$autreCommuneIndex, count($carpoolExportAutreCommuneArray[1]));
        $i=1;
        foreach($carpoolExportAutreCommuneArray[1] as $carpoolExportRow) {
            $templateProcessor->setValue("mo1#$autreCommuneIndex#$i", $carpoolExportRow[0], $autreCommuneIndex);
            $templateProcessor->setValue("tu1#$autreCommuneIndex#$i", $carpoolExportRow[1], $autreCommuneIndex);
            $templateProcessor->setValue("we1#$autreCommuneIndex#$i", $carpoolExportRow[2], $autreCommuneIndex);
            $templateProcessor->setValue("th1#$autreCommuneIndex#$i", $carpoolExportRow[3], $autreCommuneIndex);
            $templateProcessor->setValue("fr1#$autreCommuneIndex#$i", $carpoolExportRow[4], $autreCommuneIndex);
            $templateProcessor->setValue("sa1#$autreCommuneIndex#$i", $carpoolExportRow[5], $autreCommuneIndex);
            $templateProcessor->setValue("su1#$autreCommuneIndex#$i", $carpoolExportRow[6], $autreCommuneIndex);
            $templateProcessor->setValue("driverAlias1#$autreCommuneIndex#$i", $carpoolExportRow[7], $autreCommuneIndex);
            $i++;
        }
        
        $templateProcessor->cloneRow('mo2#'.$autreCommuneIndex, count($carpoolExportAutreCommuneArray[2]));
        $i=1;
        foreach($carpoolExportAutreCommuneArray[2] as $carpoolExportRow) {
            $templateProcessor->setValue("mo2#$autreCommuneIndex#$i", $carpoolExportRow[0], $autreCommuneIndex);
            $templateProcessor->setValue("tu2#$autreCommuneIndex#$i", $carpoolExportRow[1], $autreCommuneIndex);
            $templateProcessor->setValue("we2#$autreCommuneIndex#$i", $carpoolExportRow[2], $autreCommuneIndex);
            $templateProcessor->setValue("th2#$autreCommuneIndex#$i", $carpoolExportRow[3], $autreCommuneIndex);
            $templateProcessor->setValue("fr2#$autreCommuneIndex#$i", $carpoolExportRow[4], $autreCommuneIndex);
            $templateProcessor->setValue("sa2#$autreCommuneIndex#$i", $carpoolExportRow[5], $autreCommuneIndex);
            $templateProcessor->setValue("su2#$autreCommuneIndex#$i", $carpoolExportRow[6], $autreCommuneIndex);
            $templateProcessor->setValue("driverAlias2#$autreCommuneIndex#$i", $carpoolExportRow[7], $autreCommuneIndex);
            $i++;
        }
        
        $autreCommuneIndex++;
    }

    echo date('H:i:s'), ' Saving the result document...', EOL;
    $docxLocalFilePath = $LOCAL_TEMP_FOLDER . $territory_id . '.docx';
    $pdfLocalFilePath = $LOCAL_TEMP_FOLDER . $territory_id . '.pdf';
    $templateProcessor->saveAs($docxLocalFilePath);

    echo getEndingNotes(['Word2007' => 'docx'], "$LOCAL_TEMP_FOLDER$territory_id.docx");
/* Conversion PDF annulé car impossible d'installer pandoc sur le vieux serveur
    echo "Conversion to PDF... ";
    if (shell_exec("pandoc $LOCAL_TEMP_FOLDER$territory_id.docx -o $LOCAL_TEMP_FOLDER$territory_id.pdf")) {
        echo "done.\n";
    } else {
        echo "failed!!\n";
    }
*/
}



/*
	Send the JSON file to the server through FTP
*/


// Mise en place d'une connexion basique
$ftp = ftp_connect($FTP_HOSTNAME);

// Identification avec un nom d'utilisateur et un mot de passe
$login_result = ftp_login($ftp, $FTP_USERNAME, $FTP_PASSWORD);

ftp_pasv($ftp, true); 
// Vérification de la connexion
if ((!$ftp) || (!$login_result)) {
    echo "La connexion FTP a échoué !";
    echo "Tentative de connexion au serveur $FTP_HOSTNAME pour l'utilisateur $FTP_USERNAME";
    exit;
} else {
    echo "Connexion réussie au serveur $FTP_HOSTNAME, pour l'utilisateur $FTP_USERNAME\n";
}

// Chargement du fichier GeoJSON
$upload = ftp_put($ftp, $filename, $local_filepath, FTP_BINARY);

// Vérification du status du chargement
if (!$upload) {
    echo "Le chargement FTP a échoué!\n";
} else {
    echo "Chargement de $local_filepath vers $FTP_HOSTNAME en tant que $filename\n";
}


// Chargement du fichier pdf
//$upload = ftp_put($ftp, "$territory_id.pdf", $pdfLocalFilePath, FTP_BINARY);
$upload = ftp_put($ftp, "ListeCovoituragesReguliers_$territory_id.docx", $docxLocalFilePath, FTP_BINARY);

// Vérification du status du chargement
if (!$upload) {
    echo "Le chargement FTP a échoué!\n";
} else {
    //echo "Chargement de $pdfLocalFilePath vers $FTP_HOSTNAME en tant que $territory_id.pdf\n";
    echo "Chargement de $docxLocalFilePath vers $FTP_HOSTNAME en tant que ListeCovoituragesReguliers_$territory_id.docx\n";
}


// Fermeture de la connexion FTP
ftp_close($ftp);



/*
	Destroy temporary file
*/

unlink($local_filepath);

unlink($docxLocalFilePath);
//unlink($pdfLocalFilePath);



?>
